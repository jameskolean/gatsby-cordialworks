const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  const productTemplate = path.resolve(`src/templates/product-template.tsx`)
  const productTypeTemplate = path.resolve(
    `src/templates/product-type-template.tsx`
  )

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        filter: {
          frontmatter: { template: { regex: "/^(Product)|(ProductType)$/" } }
        }
      ) {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              title
              template
            }
          }
        }
      }
    }
  `)

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    switch (node.frontmatter.template) {
      case "Product":
        createPage({
          path: node.fields.slug,
          component: productTemplate,
          context: {}, // additional data can be passed via context
        })
        break
      case "ProductType":
        createPage({
          path: node.fields.slug,
          component: productTypeTemplate,
          context: {
            productType: node.frontmatter.title,
          },
        })
        break
      default:
        break
    }
  })
}
