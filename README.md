# Cordial Works

# Description

# Usage

# Running NetlifyCMS locally

To run NetlifyCMS locally you need to make some changes to it's configuration. These change must never be checked into master.

open ./static/admin/config.xml and make these edits at the top of the file

```
backend:
  name: proxy
  proxy_url: http://localhost:8081/api/v1
  branch: master
  # name: gitlab
  # repo: avenue-boutique/gatsby-shop-local
media_folder: static/assets
public_folder: /assets
# publish_mode: editorial_workflow
collections:
 ...

```

then you need to run: `npx netlify-cms-proxy-server`
