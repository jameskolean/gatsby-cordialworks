import React from "react"
import { graphql } from "gatsby"
import { Helmet } from "react-helmet"
import Grid from "@material-ui/core/Grid"
import Layout from "../components/layout"
import ProductCard from "../components/product-card"
import PageTitle from "../components/page-title"
import { Box } from "@material-ui/core"

export default function Template({
  data: {
    productType,
    site: { siteMetadata: site },
    productNodes: { nodes: products },
  },
}) {
  const lang = "en"
  const title = site?.title
  const metaDescription = site?.description
  return (
    <Layout>
      <Helmet
        htmlAttributes={{
          lang,
        }}
        title={productType.frontmatter.title}
        titleTemplate={`%s | ${title}`}
        meta={[
          {
            name: `description`,
            content: metaDescription,
          },
          {
            property: `og:title`,
            content: title,
          },
          {
            property: `og:description`,
            content: metaDescription,
          },
          {
            property: `og:type`,
            content: `website`,
          },
          {
            name: `robots`,
            content: "noindex",
          },
          {
            name: `googlebot`,
            content: "noindex",
          },
        ]}
      />
      <PageTitle title={productType.frontmatter.title} />
      <Grid container>
        {products.map(product => (
          <Box m={3} key={product.id}>
            <ProductCard product={product} />
          </Box>
        ))}
      </Grid>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!, $productType: String!) {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
    productType: markdownRemark(fields: { slug: { eq: $path } }) {
      id
      frontmatter {
        title
        description
      }
    }
    productNodes: allMarkdownRemark(
      filter: {
        frontmatter: {
          template: { eq: "Product" }
          productType: { eq: $productType }
        }
      }
      sort: { fields: frontmatter___title, order: ASC }
    ) {
      nodes {
        id
        excerpt(pruneLength: 250)
        fields {
          slug
        }
        frontmatter {
          price
          productType
          shortDescription
          sku
          tags
          thumbnail {
            childImageSharp {
              fluid(maxWidth: 600) {
                ...GatsbyImageSharpFluid
              }
              resize(width: 80) {
                src
              }
            }
          }
          title
          variant1Name
          variant1Options
        }
        html
        rawMarkdownBody
      }
    }
  }
`
