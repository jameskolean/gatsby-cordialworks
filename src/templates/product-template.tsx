import React, { useState } from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import { Helmet } from "react-helmet"
import Box from "@material-ui/core/Box"
import Typography from "@material-ui/core/Typography"
import Modal from "@material-ui/core/Modal"
// import { Button } from "gatsby-theme-material-ui"
// import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import FormLabel from "@material-ui/core/FormLabel"
import FormControl from "@material-ui/core/FormControl"
import RadioGroup from "@material-ui/core/RadioGroup"
import Radio from "@material-ui/core/Radio"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import GridList from "@material-ui/core/GridList"
import GridListTile from "@material-ui/core/GridListTile"

import Layout from "../components/layout"
import PageTitle from "../components/page-title"
import Markdown from "../utils/markdown"
import format from "../utils/format"
//import { string } from "prop-types"
import ContactFormDialog from "../components/contact-form-dialog"

const useStyles = makeStyles(
  ({ palette, shadows, spacing, breakpoints }: Theme) =>
    createStyles({
      root: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        backgroundColor: palette.background.paper,
      },
      gridList: {
        width: "100%",
      },
      paper: {
        position: "absolute",
        width: "90%",
        backgroundColor: palette.background.paper,
        border: "2px solid #000",
        boxShadow: shadows[5],
        padding: spacing(2, 4, 3),
      },
      sectionDesktop: {
        [breakpoints.down("sm")]: {
          display: "none",
        },
      },
      sectionMobile: {
        [breakpoints.up("md")]: {
          display: "none",
        },
      },
    })
)

function getModalStyle() {
  const top = 50
  const left = 50

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  }
}

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const classes = useStyles()
  const product = data.markdownRemark // data.markdownRemark holds your post data
  const [option1, setOption1] = useState(
    product.frontmatter.variant1Options
      ? product.frontmatter.variant1Options[0]
      : ""
  )
  const handleOption1Change = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOption1((event.target as HTMLInputElement).value)
  }

  const [modalStyle] = React.useState(getModalStyle)
  const [open, setOpen] = React.useState(false)
  const [zoomedImage, setZoomedImage] = React.useState("")
  const handleOpen = (image: string) => {
    setZoomedImage(image)
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const body = (
    <div style={modalStyle} className={classes.paper}>
      <img src={zoomedImage} />
    </div>
  )
  const Gallery = () => {
    if (!product.frontmatter.gallery) return null
    return (
      <div className={classes.root}>
        <GridList cellHeight={160} className={classes.gridList} cols={5}>
          {product.frontmatter.gallery.map(galleryImage => (
            <GridListTile
              key={galleryImage.image.childImageSharp.id}
              cols={1}
              onClick={() =>
                handleOpen(galleryImage.image.childImageSharp.largeFixed.src)
              }
            >
              <Img
                fixed={galleryImage.image.childImageSharp.smallFixed}
                alt={galleryImage.image.alt}
              />
            </GridListTile>
          ))}
        </GridList>
      </div>
    )
  }
  const lang = "en"
  const site = data.site.siteMetadata
  const title = site?.title
  const metaDescription = site?.description
  return (
    <Layout>
      <Helmet
        htmlAttributes={{
          lang,
        }}
        title={product.frontmatter.title}
        titleTemplate={`%s | ${title}`}
        meta={[
          {
            name: `description`,
            content: metaDescription,
          },
          {
            property: `og:title`,
            content: title,
          },
          {
            property: `og:description`,
            content: metaDescription,
          },
          {
            property: `og:type`,
            content: `website`,
          },
          {
            name: `robots`,
            content: "noindex",
          },
          {
            name: `googlebot`,
            content: "noindex",
          },
        ]}
      />
      <PageTitle title={product.frontmatter.title} />
      <Box display="flex" p={1}>
        <Box className={classes.sectionDesktop} p={1} flexGrow={1}>
          <Img fixed={product.frontmatter.thumbnail.childImageSharp.fixed} />
          <Typography>
            {format.formatCurrency(product.frontmatter.price)}
          </Typography>
          <Box pt={2}>
            <FormControl component="fieldset">
              <FormLabel component="legend">
                {product.frontmatter.variant1Name}
              </FormLabel>
              <RadioGroup
                aria-label="gender"
                name="gender1"
                value={option1}
                onChange={handleOption1Change}
              >
                {product.frontmatter.variant1Options &&
                  product.frontmatter.variant1Options.map(option => (
                    <FormControlLabel
                      key={option}
                      label={option}
                      control={
                        <Radio size="small" color="default" value={option} />
                      }
                    />
                  ))}
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
        <Box p={1} flexGrow={1} width={"100%"}>
          <Box className={classes.sectionMobile} p={1} flexGrow={1}>
            <Img fluid={product.frontmatter.thumbnail.childImageSharp.fluid} />
            <Typography>
              {format.formatCurrency(product.frontmatter.price)}
            </Typography>
            <Box pt={2}>
              <FormControl component="fieldset">
                <FormLabel component="legend">
                  {product.frontmatter.variant1Name}
                </FormLabel>
                <RadioGroup
                  aria-label="gender"
                  name="gender1"
                  value={option1}
                  onChange={handleOption1Change}
                >
                  {product.frontmatter.variant1Options &&
                    product.frontmatter.variant1Options.map(option => (
                      <FormControlLabel
                        key={option}
                        label={option}
                        control={
                          <Radio size="small" color="default" value={option} />
                        }
                      />
                    ))}
                </RadioGroup>
              </FormControl>
            </Box>
          </Box>

          <ContactFormDialog
            product={product}
            option1={option1}
            setOption1={setOption1}
          />
          <Typography>{product.frontmatter.shortDescription}</Typography>
          <Markdown>{product.rawMarkdownBody}</Markdown>
          <Gallery />
        </Box>
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $path } }) {
      id
      excerpt(pruneLength: 250)
      fields {
        slug
      }
      frontmatter {
        price
        productType
        shortDescription
        sku
        tags
        thumbnail {
          childImageSharp {
            fixed(width: 200) {
              ...GatsbyImageSharpFixed
            }
            fluid(maxWidth: 400) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        gallery {
          alt
          image {
            childImageSharp {
              id
              smallFixed: fixed(width: 100) {
                ...GatsbyImageSharpFixed
              }
              largeFixed: fixed(width: 900) {
                ...GatsbyImageSharpFixed
              }
            }
            childMarkdownRemark {
              id
            }
          }
        }

        title
        variant1Name
        variant1Options
      }
      html
      rawMarkdownBody
    }
  }
`
