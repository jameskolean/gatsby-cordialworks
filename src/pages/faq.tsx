import React from "react"
import { graphql } from "gatsby"
import { makeStyles } from "@material-ui/core/styles"
import { Box, Typography } from "@material-ui/core"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PageTitle from "../components/page-title"
import ExpansionPanel from "@material-ui/core/ExpansionPanel"
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary"
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"

const useStyles = makeStyles(theme => ({
  root: {},
  heading: {
    fontSize: theme.typography.fontSize.large,
    fontWeight: theme.typography.fontWeightBold,
  },
}))
const FaqPage = ({
  data: {
    page: {
      frontmatter: { questions },
    },
  },
}) => {
  // const classes = useStyles()
  const classes = useStyles()
  return (
    <Layout>
      <SEO title="Frequently Asked Questions" />
      <PageTitle title="Frequently Asked Questions" />
      <Box m={3}>
        {questions.map((question, questionIndex) => (
          <ExpansionPanel key={questionIndex}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls={`panel${questionIndex}a-content`}
              id={`pane${questionIndex}1a-header`}
            >
              <Typography className={classes.heading}>
                {question.question}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>{question.answer}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </Box>
    </Layout>
  )
}

export default FaqPage

export const pageQuery = graphql`
  query faqPageQuery {
    page: markdownRemark(
      frontmatter: {
        title: { eq: "Frequently Asked Questions" }
        template: { eq: "Component" }
      }
    ) {
      frontmatter {
        questions {
          answer
          question
        }
      }
    }
  }
`
