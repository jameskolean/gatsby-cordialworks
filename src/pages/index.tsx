import React from "react"
import TypeBackground from "../types/createPalette"
import { graphql } from "gatsby"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import { Container, Grid, GridList, Paper } from "@material-ui/core"
// import {
//   BottomNavigationAction,
//   Button,
//   CardActionArea,
//   Fab,
//   IconButton,
//   Link,
// } from "gatsby-theme-material-ui"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ProductCard from "../components/product-card"
import { Typography } from "@material-ui/core"

import Hero from "../components/hero"
import Categories from "../components/categories"

const useStyles = makeStyles(({ palette, spacing }: Theme) =>
  createStyles({
    productGroup: {
      background: palette.grey.A100,
      marginTop: spacing(3),
      padding: spacing(1),
    },
    rootContainer: {
      marginTop: spacing(3),
      paddingLeft: spacing(3),
      paddingRight: spacing(3),
      marginLeft: "auto",
      marginRight: "auto",
    },
    instructions: {},
    sectionTitle: {
      color: palette.grey[900],
      paddingBottom: spacing(1),
      // borderBottom: `2px ${palette.primary1Color} solid`,
      borderBottom: `3px solid #e4e4e4`,
      fontWeight: 400,
      fontSize: 22,
      letterSpacing: "1px",
    },
  })
)

const groupBy = (list, keyGetter) => {
  const map = new Map()
  list.forEach(item => {
    const key = keyGetter(item)
    const collection = map.get(key)
    if (!collection) {
      map.set(key, [item])
    } else {
      collection.push(item)
    }
  })
  return map
}

const IndexPage = ({
  data: {
    allMarkdownRemark: { nodes: products },
  },
}) => {
  const classes = useStyles()
  const productGroups = groupBy(
    products,
    product => product.frontmatter.productType
  )
  const ProductGroup = ({ title, products }) => (
    <Container>
      <Paper variant="outlined" square className={classes.productGroup}>
        <Typography variant="h3">{title}</Typography>
        <GridList>
          {products.map(product => (
            <ProductCard key={product.id} product={product} />
          ))}
        </GridList>
      </Paper>
    </Container>
  )

  return (
    <Layout>
      <SEO title="Home" />
      <Hero />
      <Categories />
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query indexPageQuery {
    allMarkdownRemark(
      filter: { frontmatter: { template: { eq: "Product" } } }
      sort: { fields: frontmatter___productType, order: ASC }
    ) {
      nodes {
        id
        excerpt(pruneLength: 200, format: HTML)
        fields {
          slug
        }
        frontmatter {
          price
          productType
          shortDescription
          sku
          tags
          thumbnail {
            childImageSharp {
              fixed(width: 200) {
                ...GatsbyImageSharpFixed
              }
              # resize(width: 200) {
              #   src
              # }
            }
          }
          title
          variant1Name
          variant1Options
        }
        html
        rawMarkdownBody
      }
    }
  }
`
