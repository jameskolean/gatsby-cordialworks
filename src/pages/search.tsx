import React, { useState } from "react"
import { graphql } from "gatsby"
import clsx from "clsx"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import { Index } from "elasticlunr"
import {
  Box,
  Typography,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  List,
  ListItem,
} from "@material-ui/core"
import SearchIcon from "@material-ui/icons/Search"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PageTitle from "../components/page-title"
import { Link } from "gatsby-theme-material-ui"
import format from "../utils/format"

const useStyles = makeStyles(({ palette, spacing }: Theme) =>
  createStyles({
    rootContainer: {
      padding: spacing(2),
    },
    margin: {
      margin: spacing(1),
    },
    withoutLabel: {
      marginTop: spacing(3),
    },
    textField: {
      width: "25ch",
    },
  })
)

const SearchPage = ({ data: { siteSearchIndex } }) => {
  const classes = useStyles()
  const index = Index.load(siteSearchIndex.index)

  const [searchResults, setSearchResults] = useState({
    query: ``,
    results: [],
  })
  const search = event => {
    //    this.index = this.getOrCreateIndex()
    const query = event.target.value
    setSearchResults({
      query,
      // Query the index with search string to get an [] of IDs
      results: index
        .search(query, { expand: true })
        // Map over each ID and return the full document
        .map(({ ref }) => index.documentStore.getDoc(ref)),
    })
  }
  return (
    <Layout>
      <SEO title="Search" />
      <PageTitle title="Search Results For ..." />
      <Box className={classes.rootContainer}>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type="text"
            value={searchResults.query}
            onChange={search}
            endAdornment={
              <InputAdornment variant="filled" position="end">
                <SearchIcon />
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
      </Box>
      <Box className={classes.rootContainer}>
        <List>
          {searchResults.results.map(item => (
            <ListItem>
              <Link to={item.slug}>
                <Typography>
                  <strong>{item.title}</strong>{" "}
                  {format.formatCurrency(item.price)}
                </Typography>
              </Link>
            </ListItem>
          ))}
        </List>
      </Box>
    </Layout>
  )
}

export default SearchPage

export const pageQuery = graphql`
  query searchPageQuery {
    siteSearchIndex {
      index
    }
  }
`
