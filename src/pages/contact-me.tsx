import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { Box } from "@material-ui/core"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Markdown from "../utils/markdown"
import PageTitle from "../components/page-title"

const ContactMePage = () => {
  // const classes = useStyles()

  return (
    <Layout>
      <SEO title="Contact Me" />
      <PageTitle title="Contact Me" />
      <h1>Contact form goes here</h1>
    </Layout>
  )
}

export default ContactMePage
