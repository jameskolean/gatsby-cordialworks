import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { Box } from "@material-ui/core"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Markdown from "../utils/markdown"
import PageTitle from "../components/page-title"

const AboutMePage = ({ data: { page, storeInfo } }) => {
  // const classes = useStyles()

  return (
    <Layout>
      <SEO title="About Us" />
      <PageTitle title="About Me" />
      <Box m={3}>
        <Markdown>{page.rawMarkdownBody}</Markdown>
      </Box>
    </Layout>
  )
}

export default AboutMePage

// const useStyles = makeStyles(({ palette, spacing }) =>
//   createStyles({
//     title: {
//       margin: spacing(2),
//       color: palette.primary1Color,
//     },
//   })
// )

export const pageQuery = graphql`
  query aboutMePageQuery {
    storeInfo: markdownRemark(
      frontmatter: {
        title: { eq: "Store Information" }
        template: { eq: "Component" }
      }
    ) {
      frontmatter {
        heroImage {
          childImageSharp {
            fluid(maxWidth: 900) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    page: markdownRemark(
      frontmatter: {
        title: { eq: "About Us Page" }
        template: { eq: "Component" }
      }
    ) {
      rawMarkdownBody
    }
  }
`
