import React from "react"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql, navigate, Link } from "gatsby"
import {
  fade,
  makeStyles,
  Theme,
  createStyles,
  withStyles,
} from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Box from "@material-ui/core/Box"
import Menu, { MenuProps } from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem"
import Toolbar from "@material-ui/core/Toolbar"
import MoreIcon from "@material-ui/icons/MoreVert"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import {
  Button,
  // BottomNavigationAction,
  // CardActionArea,
  // Fab,
  IconButton,
  // Link,
} from "gatsby-theme-material-ui"

const useStyles = makeStyles(({ breakpoints, palette, spacing }: Theme) =>
  createStyles({
    appbar: {
      background: palette.background.appbar,
    },
    grow: {
      flexGrow: 1,
    },
    title: {
      fontFamily: '"Permanent Marker", "Roboto", sans-serif',
      color: "white",
    },
    sectionDesktop: {
      display: "none",
      [breakpoints.up("sm")]: {
        display: "flex",
      },
    },
    sectionMobile: {
      display: "flex",
      [breakpoints.up("sm")]: {
        display: "none",
      },
    },
    menuItem: {
      color: palette.grey[100],
    },
  })
)

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
))

export default function AppBarMain() {
  const classes = useStyles()
  const [stuffAnchorEl, setStuffAnchorEl] = React.useState<null | HTMLElement>(
    null
  )
  const [infoAnchorEl, setInfoAnchorEl] = React.useState<null | HTMLElement>(
    null
  )
  const [
    mobileMoreAnchorEl,
    setMobileMoreAnchorEl,
  ] = React.useState<null | HTMLElement>(null)
  const isStuffMenuOpen = Boolean(stuffAnchorEl)
  const isInfofMenuOpen = Boolean(infoAnchorEl)
  const isMobileMoreMenuOpen = Boolean(mobileMoreAnchorEl)

  const handleStuffMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setStuffAnchorEl(event.currentTarget)
  }
  const handleInfoMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setInfoAnchorEl(event.currentTarget)
  }
  const handleMobileMoreMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const handleStuffMenuClose = () => {
    setStuffAnchorEl(null)
  }
  const handleInfoMenuClose = () => {
    setInfoAnchorEl(null)
  }
  const handleMobileMoreMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  const stuffMenuId = "stuff-menu"
  const infoMenuId = "info-mobile"
  const mobileMenuId = "menu-mobile"

  const renderStuffMenu = (
    <StyledMenu
      id={stuffMenuId}
      anchorEl={stuffAnchorEl}
      keepMounted
      open={isStuffMenuOpen}
      onClose={handleStuffMenuClose}
    >
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/search/")
        }}
      >
        Search
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleStuffMenuClose()
          navigate("/product-type/cutlery")
        }}
      >
        Cuttlery
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleStuffMenuClose()
          navigate("/product-type/metal-art/")
        }}
      >
        Metal Art
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleStuffMenuClose()
          navigate("/product-type/pottery")
        }}
      >
        Pottery
      </MenuItem>
    </StyledMenu>
  )
  const renderInfoMenu = (
    <StyledMenu
      id={infoMenuId}
      anchorEl={infoAnchorEl}
      keepMounted
      open={isInfofMenuOpen}
      onClose={handleInfoMenuClose}
    >
      <MenuItem
        onClick={() => {
          handleInfoMenuClose()
          navigate("/about-me/")
        }}
      >
        About Me
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleInfoMenuClose()
          navigate("/contact-me")
        }}
      >
        Contact Me
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleInfoMenuClose()
          navigate("/faq")
        }}
      >
        FAQ
      </MenuItem>
    </StyledMenu>
  )

  const renderMobileMenu = (
    <StyledMenu
      id={mobileMenuId}
      anchorEl={mobileMoreAnchorEl}
      keepMounted
      open={isMobileMoreMenuOpen}
      onClose={handleMobileMoreMenuClose}
    >
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/search/")
        }}
      >
        Search
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/product-type/cutlery")
        }}
      >
        Cuttlery
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/product-type/metal-art/")
        }}
      >
        Metal Art
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/product-type/pottery")
        }}
      >
        Pottery
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/about-me")
        }}
      >
        About Me
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/contact-me")
        }}
      >
        Contact Me
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleMobileMoreMenuClose()
          navigate("/faq")
        }}
      >
        FAQ
      </MenuItem>
    </StyledMenu>
  )

  return (
    <>
      <Helmet>
        <link
          href="https://fonts.googleapis.com/css?family=Permanent+Marker"
          rel="stylesheet"
        />
      </Helmet>

      <AppBar position="sticky" className={classes.appbar}>
        <Toolbar>
          <Button
            size="large"
            onClick={() => navigate("/")}
            className={classes.title}
          >
            {data.site.siteMetadata.title}
          </Button>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <Button
              className={classes.menuItem}
              aria-controls={stuffMenuId}
              aria-haspopup="true"
              onClick={handleStuffMenuOpen}
              startIcon={<ExpandMoreIcon />}
            >
              stuff
            </Button>
            <Button
              className={classes.menuItem}
              aria-controls={infoMenuId}
              aria-haspopup="true"
              onClick={handleInfoMenuOpen}
              startIcon={<ExpandMoreIcon />}
            >
              info
            </Button>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMoreMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderStuffMenu}
      {renderInfoMenu}
    </>
  )
}
