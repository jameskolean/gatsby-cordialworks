import React from "react"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"

import CookieConsent from "react-cookie-consent"
import AppBar from "./app-bar"
import "./layout.css"
import { Box } from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    copyright: {
      fontSize: "0.6rem",
    },
    divider: {
      color: "white",
      marginTop: "1rem",
      height: "0.1rem",
      backgroundColor: "black",
    },
  })
)

const Layout = ({ children }) => {
  const classes = useStyles()

  const data = useStaticQuery(graphql`
    query SiteConsentCookieQuery {
      site {
        siteMetadata {
          cookieConsent
        }
      }
    }
  `)

  return (
    <>
      <AppBar />
      <main>{children}</main>
      <footer>
        <Divider className={classes.divider} />
        <Box display="block" style={{ float: "left" }}>
          <Typography
            variant="caption"
            display="block"
            className={classes.copyright}
          >
            © {new Date().getFullYear()}, Code Green LLC
          </Typography>
        </Box>
        <Box display="block" style={{ float: "right" }}>
          <Typography
            variant="caption"
            display="inline"
            className={classes.copyright}
          >
            Narwhal Icon made by
          </Typography>
          <a href="https://www.flaticon.com/authors/freepik" title="Freepik">
            <Typography
              variant="caption"
              display="inline"
              className={classes.copyright}
            >
              Freepik
            </Typography>
          </a>
          <Typography
            variant="caption"
            display="inline"
            className={classes.copyright}
          >
            from
          </Typography>
          <a href="https://www.flaticon.com/" title="Flaticon">
            <Typography
              variant="caption"
              display="inline"
              className={classes.copyright}
            >
              www.flaticon.com
            </Typography>
          </a>
        </Box>
      </footer>
      <CookieConsent>{data.site.siteMetadata.cookieConsent}</CookieConsent>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
