import React from "react"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import Box from "@material-ui/core/Box"
//import TypeBackground from "../types/createPalette"

const useStyles = makeStyles(({ breakpoints, palette, spacing }: Theme) =>
  createStyles({
    root: {
      background: palette.background.appbar,
      color: "white",
      fontSize: "1.5rem",
    },
    title: {
      [breakpoints.up("sm")]: {
        fontSize: "4rem",
      },
      [breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      fontWeight: "bold",
    },
    subtitle: {
      [breakpoints.up("sm")]: {
        fontSize: "2rem",
      },
      [breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
      fontWeight: "bold",
    },
    narwhalMobile: {
      [breakpoints.down("sm")]: {
        display: "inline",
        transform: "translateY(50%)",
        marginRight: spacing(1),
      },
      display: "none",
    },
    narwhalDestktop: {
      [breakpoints.down("sm")]: {
        display: "none",
      },
    },
  })
)

const Hero = () => {
  const classes = useStyles()

  return (
    <Box className={classes.root} p={3}>
      <Grid container>
        <Grid item xs={12} md={8}>
          <Box mt={5} mb={8}>
            <img
              className={classes.narwhalMobile}
              src="./assets/narwhal-white.svg"
              alt="Narwhal"
              height="50"
              width="50"
            />
            <Typography style={{ display: "inline" }} className={classes.title}>
              Cordial Works
            </Typography>
            <Typography className={classes.subtitle}>
              Hand crafted art you can use. If you want art in your life
              consider these items.
            </Typography>
          </Box>
        </Grid>
        <Grid item md={4}>
          <Box ml={5}>
            <img
              className={classes.narwhalDestktop}
              src="./assets/narwhal-white.svg"
              alt="Narwhal"
              height="250"
              width="250"
            />
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

export default Hero
