import React from "react"
import { makeStyles, createStyles } from "@material-ui/core/styles"
import { Typography } from "@material-ui/core"
import TypeBackground from "../../types/createPalette"

const PageTitle = ({ title }) => {
  const classes = useStyles()

  return (
    <Typography align="center" className={classes.root} variant="h5">
      {title}
    </Typography>
  )
}

export default PageTitle

const useStyles = makeStyles(({ palette, spacing }) =>
  createStyles({
    root: {
      background: palette.background.appbar,
      color: "white",
      textTransform: "uppercase",
      fontSize: "3rem",
      fontWeight: "bold",
      paddingBottom: spacing(3),
    },
  })
)
