import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"
import Img from "gatsby-image"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import CardMedia from "@material-ui/core/CardMedia"
import CardContent from "@material-ui/core/CardContent"
import Typography from "@material-ui/core/Typography"
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart"
import {
  BottomNavigationAction,
  Button,
  CardActionArea,
  Fab,
  IconButton,
  Link,
} from "gatsby-theme-material-ui"
import ContactFormDialog from "./contact-form-dialog"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.up("sm")]: {
        display: "flex",
      },
      [theme.breakpoints.down("sm")]: {
        display: "block",
      },
    },
    details: {
      display: "flex",
      flexDirection: "column",
    },
    content: {
      flex: "1 0 auto",
    },
    thumbnail: {
      width: "100%",
    },
    controls: {
      display: "flex",
      alignItems: "center",
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
  })
)

export default function ProductCard({ product }) {
  const classes = useStyles()
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
  })
  const { title, shortDescription } = product.frontmatter
  return (
    <Card className={classes.root}>
      {product.frontmatter.thumbnail && (
        <CardMedia
          className={classes.thumbnail}
          onClick={() => navigate(product.fields.slug)}
        >
          <Img
            fluid={product.frontmatter.thumbnail.childImageSharp.fluid}
            alt={title + " - Featured Shot"}
          />
          {/* <ContactFormDialog product={product} /> */}
        </CardMedia>
      )}
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Link to={product.fields.slug}>
            <Typography variant="h5">{title}</Typography>
          </Link>
          <Typography variant="subtitle1" color="textSecondary">
            {shortDescription}
          </Typography>
          <Typography variant="body2" component="div">
            <div dangerouslySetInnerHTML={{ __html: product.excerpt }} />
          </Typography>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            to={product.fields.slug}
          >
            Details
          </Button>
        </CardContent>
      </div>
    </Card>
  )
}
