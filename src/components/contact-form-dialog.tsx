import React, { useState, useRef } from "react"
//import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import TextField from "@material-ui/core/TextField"
import Dialog from "@material-ui/core/Dialog"
import Box from "@material-ui/core/Box"
import DialogContent from "@material-ui/core/DialogContent"
import DialogContentText from "@material-ui/core/DialogContentText"
import DialogTitle from "@material-ui/core/DialogTitle"
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart"
import FormLabel from "@material-ui/core/FormLabel"
import FormControl from "@material-ui/core/FormControl"
import RadioGroup from "@material-ui/core/RadioGroup"
import Radio from "@material-ui/core/Radio"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import { Button, IconButton } from "gatsby-theme-material-ui"

export default function ContactFormDialog({ product, option1, setOption1 }) {
  const formEl = useRef(null)
  const [open, setOpen] = useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const [email, setEmail] = useState("")
  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail((event.target as HTMLInputElement).value)
  }

  const handleOption1Change = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOption1((event.target as HTMLInputElement).value)
  }
  const Variant1 = () => {
    if (!product.frontmatter.variant1Options) return null
    return (
      <Box pt={2}>
        <FormControl component="fieldset">
          <FormLabel component="legend">
            {product.frontmatter.variant1Name}
          </FormLabel>
          <RadioGroup
            aria-label="gender"
            name="gender1"
            value={option1}
            onChange={handleOption1Change}
          >
            {product.frontmatter.variant1Options &&
              product.frontmatter.variant1Options.map(option => (
                <FormControlLabel
                  key={option}
                  control={
                    <Radio size="small" color="default" value={option} />
                  }
                  label={option}
                />
              ))}
          </RadioGroup>
        </FormControl>
      </Box>
    )
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    formEl.current.submit()
  }
  return (
    <div>
      <Button
        aria-label="add to cart"
        variant="contained"
        color="primary"
        fullWidth
        onClick={handleClickOpen}
      >
        Contact me to arrange purchase.
      </Button>
      {/* Don't put the form in the Dialog, Netlify can't find it at build time if you do */}
      {/* To initialize a form the field can't be hidden */}
      <form
        ref={formEl}
        name="purchase-inquiry"
        method="post"
        data-netlify="true"
        data-netlify-honeypot="bot-field"
      >
        <input type="hidden" name="bot-field" />
        <input type="hidden" name="form-name" value="purchase-inquiry" />
        <input type="hidden" name="sku" value={product.frontmatter.sku} />
        <input type="hidden" name="buyer-email" value={email} />
        <input type="hidden" name="product" value={product.frontmatter.title} />
        <input type="hidden" name="variant1" value={option1} />
        <input
          type="hidden"
          name="message"
          value={`purchase request from ${email} for ${product.frontmatter.title}`}
        />
      </form>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Purchase</DialogTitle>
        <DialogContent>
          <Typography variant="h6">
            Product: {product.frontmatter.title}
          </Typography>

          <Typography variant="body1">
            To purchase this item, please enter your email address here.
          </Typography>
          <Typography variant="body1">
            We will contact you with delivery and payment options.
          </Typography>
          <Variant1 />
          <TextField
            autoFocus
            margin="dense"
            id="email"
            name="email"
            label="Email Address"
            type="email"
            onChange={handleEmailChange}
            fullWidth
          />
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleFormSubmit}>Send</Button>
        </DialogContent>
      </Dialog>
    </div>
  )
}
