import React from "react"
import { useStaticQuery, graphql, navigate } from "gatsby"
import {
  Button,
  // BottomNavigationAction,
  // CardActionArea,
  // Fab,
  //  IconButton,
  // Link,
} from "gatsby-theme-material-ui"

import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardActions from "@material-ui/core/CardActions"
import CardContent from "@material-ui/core/CardContent"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import Box from "@material-ui/core/Box"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import Img from "gatsby-image"

const useStyles = makeStyles(({ breakpoints, palette, spacing }: Theme) =>
  createStyles({
    title: {
      fontSize: "3rem",
      fontWeight: "bold",
    },
    subtitle: {
      fontSize: "1.5rem",
      fontWeight: "bold",
    },
    category: {
      maxWidth: 350,
      margin: "auto",
    },
  })
)
const CategoriesCard = ({ category }) => {
  const classes = useStyles()

  return (
    <Card className={classes.category}>
      <CardActionArea onClick={() => navigate(category.fields.slug)}>
        <Img fixed={category.frontmatter.image.childImageSharp.fixed}></Img>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {category.frontmatter.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {category.frontmatter.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" to={category.fields.slug}>
          Show Me
        </Button>
      </CardActions>
    </Card>
  )
}
const Categories = () => {
  const classes = useStyles()
  const data = useStaticQuery(graphql`
    query categoriesQuery {
      allMarkdownRemark(
        filter: { frontmatter: { template: { eq: "ProductType" } } }
        sort: { fields: frontmatter___title, order: ASC }
      ) {
        nodes {
          frontmatter {
            title
            description
            image {
              childImageSharp {
                fixed(fit: COVER, width: 350, height: 200) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  `)
  const categories = data.allMarkdownRemark.nodes
  return (
    <>
      <Box m={5}>
        <Typography align="center" className={classes.title}>
          Categories
        </Typography>
        <Typography align="center" className={classes.subtitle}>
          Choose a category to explore.
        </Typography>
      </Box>

      <Grid container>
        {categories.map(category => (
          <Grid item xs={12} sm={6} md={4} key={category.fields.slug}>
            <CategoriesCard category={category} />
          </Grid>
        ))}
      </Grid>
    </>
  )
}
// Header.propTypes = {
//   siteTitle: PropTypes.string,
// }

// Header.defaultProps = {
//   siteTitle: ``,
// }

export default Categories
