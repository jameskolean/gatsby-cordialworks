import { createMuiTheme } from "@material-ui/core"

const theme = createMuiTheme({
  // palette: {
  //   type: "dark",
  // },
  palette: {
    background: {
      appbar: "#0c052e",
    },
  },
})

export default theme
