---
template: Product
date: 2020-06-02
sku: knife-8-carve
variant1Name:
variant1Options:
title: 8 inch Carving Knife
price: 115.50
productType: Cutlery
tags:
  - Knife
  - Chef
  - Handmade
thumbnail: /assets/bread-knife-unsplash.jpg
shortDescription: Handmade Carving knife with Hawiian Koa wood handle.
---

# Lorem ipsum

dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
