---
template: Product
date: 2020-06-02T00:00:00.000Z
sku: knife-8-bread
variant1Name: Handle Material
variant1Options:
  - Maple
  - Oak[+5.00]
  - Koa[+30.00]
title: 8 inch Bread Knife
price: 115.5
productType: Cutlery
tags:
  - Knife
  - Bread
  - Handmade
thumbnail: /assets/bread-knife-unsplash.jpg
gallery:
  - image: /assets/brett-jordan-xG5caIE3tLU-unsplash.jpg
    alt: stapler
  - image: /assets/dog-in-box-unsplash.jpg
    alt: puppy
  - image: /assets/kevin-ramdhun-h5zhxfMAoAQ-unsplash.jpg
    alt: clipper
  - image: /assets/jose-aljovin-OocF8znfhYQ-unsplash.jpg
    alt: staple gun
shortDescription: Handmade bread knife with Hawiian Koa wood handle.
---

# Lorem ipsum

dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
