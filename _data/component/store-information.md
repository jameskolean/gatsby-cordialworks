---
template: Component
title: Store Information
location: '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-82.87695407867433%2C42.600905238230965%2C-82.87444889545442%2C42.6023682051861&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/42.60164/-82.87570&amp;layers=N">View Larger Map</a></small>'
addressLines:
  - 100 Nowhere
  - NY, NY 12345
contacts:
  - email@noreply.com
hours:
  - days: Mon-Fri
    hours: 9am-6pm
  - days: Sat
    hours: 11am-6pm
heroImage: /assets/hero-unsplash.jpg
---
